import torch
import torchvision
import os
import cv2
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from scipy.ndimage.measurements import label
from torch import nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.utils import save_image
from torchvision.datasets import MNIST
from multiprocessing import Pool, freeze_support
from slackIntegration import logSlack
from itertools import product
from torchsummary import summary
from torchviz import make_dot

main_image_directory = "./AIA_Images"
desc_directory = "{main_image_directory}/Desc".format(main_image_directory=main_image_directory)
active_region_path_directory = "{directory}/AR".format(directory=main_image_directory)
threshold_value = 180
max_descriptor_val=0

# %% autoencoder
class autoencoder(nn.Module):
    def __init__(self):
        super(autoencoder, self).__init__()
        self.conv1 = nn.Sequential(
           nn.Conv1d(in_channels=1, out_channels=16, kernel_size=3, padding=1),
           nn.ReLU(inplace=True),
           nn.MaxPool1d(kernel_size=2,  return_indices=True, padding=0)          
        )
        self.conv2 = nn.Sequential(
           nn.Conv1d(in_channels=16, out_channels=32, kernel_size=3, padding=1),
           nn.ReLU(inplace=True),
           nn.MaxPool1d(kernel_size=2,  return_indices=True, padding=0)          
        )
        self.conv3 = nn.Sequential(
           nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, padding=1),
           nn.ReLU(inplace=True),
           nn.MaxPool1d(kernel_size=2,  return_indices=True, padding=0)          
        )
        self.conv4 = nn.Sequential(
           nn.Conv1d(in_channels=16, out_channels=1, kernel_size=3, padding=1),
           nn.ReLU(inplace=True),
           nn.MaxPool1d(kernel_size=2,  return_indices=True, padding=0)          
        )     
        
        self.unpool1 = nn.MaxUnpool1d(kernel_size=2, padding=0)
        self.deconv1 = nn.Sequential(
            nn.ConvTranspose1d(in_channels=1, out_channels=16 ,kernel_size=1, padding=0),
            nn.ReLU(inplace=True),
            #nn.ConstantPad1d( padding=(0,1), value = 0),            
            # nn.Tanh()
        )
        self.unpool2 = nn.MaxUnpool1d(kernel_size=2, padding=0)
        self.deconv2 = nn.Sequential(
            nn.ConvTranspose1d(in_channels=16, out_channels=32,kernel_size=1, padding=0),
            nn.ReLU(inplace=True),
            #nn.ConstantPad1d( padding=(0,1), value = 0),
            #nn.Tanh()
        )
        self.unpool3 = nn.MaxUnpool1d(kernel_size=2, padding=0)
        self.deconv3 = nn.Sequential(
            nn.ConvTranspose1d(in_channels=32, out_channels=16,kernel_size=1, padding=0),
            nn.ReLU(inplace=True),
            #nn.ConstantPad1d( padding=(0,1), value = 0),
           # nn.Tanh()
        )
        self.unpool4 = nn.MaxUnpool1d(kernel_size=2, padding=0)
        self.deconv4 = nn.Sequential(
            nn.ConvTranspose1d(in_channels=16, out_channels=1, kernel_size=1, padding=0),
            nn.ReLU(inplace=True),
            #nn.ConstantPad1d( padding=(0,1), value = 0),
            nn.Tanh()
        )

        self.encoded = nn.Sequential(
            nn.Conv1d(in_channels=1, out_channels=16, kernel_size=3, padding=0),
            nn.ReLU(inplace=True),
            nn.MaxPool1d(kernel_size=2, padding=0),
            nn.Conv1d(in_channels=16, out_channels=32, kernel_size=3, padding=0),
            nn.ReLU(inplace=True),
            nn.MaxPool1d(kernel_size=2, padding=0),
            nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, padding=0),
            nn.ReLU(inplace=True),
            nn.MaxPool1d(kernel_size=2, padding=0),
            nn.Conv1d(in_channels=16, out_channels=1, kernel_size=3, padding=0),
            nn.ReLU(inplace=True),
            nn.MaxPool1d(kernel_size=2, padding=0),
              
        )

    def forward(self, x):
        x, indices1 = self.conv1(x)
        x, indices2 = self.conv2(x)
        x, indices3 = self.conv3(x)
        encoded, indices4 = self.conv4(x)
        out = self.unpool1(encoded,indices4)
        out = self.deconv1(out)
        out = self.unpool2(out, indices3)
        out = self.deconv2(out)
        out = self.unpool3(out, indices2)
        out = self.deconv3(out)
        out = self.unpool4(out,indices1)
        decoded = self.deconv4(out)
        return decoded

# %% helper functions
def detect_active_regions(image_path):
    global active_region_path_directory
    global threshold_value
    if(os.path.exists(image_path)):
        # load the image, convert it to grayscale, and blur it
        image = cv2.imread(image_path)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (11, 11), 0)
        thresh = cv2.threshold(blurred, threshold_value,255, cv2.THRESH_BINARY)[1]
        thresh = cv2.erode(thresh, None, iterations=2)
        thresh = cv2.dilate(thresh, None, iterations=4)
        file_path = "{active_region_file_path}/{base_file_name}_AR.jpg".format(active_region_file_path=active_region_path_directory, base_file_name=os.path.basename(image_path))
        #plt.imsave(file_path, thresh)
        thresh = thresh / 255        
        print(file_path)
        return thresh

def calculate_descriptor(detected_bright_regions_image, device):
    active_region_tensor= torch.tensor(detected_bright_regions_image, device=device)
    thresholded_tensor = (active_region_tensor >= 0.71)
    thresholded_tensor = thresholded_tensor.type(torch.float32)
    hv = torch.sum(thresholded_tensor, dim=0)
    wv = torch.sum(thresholded_tensor, dim=1) 
    descriptor = torch.cat((hv, wv), 0)
    return descriptor

def save_encoded_hash(descriptor, file_path):
    np_desc= descriptor.cpu().detach().numpy()
    file_path = "{dst_dic}/{base_file_name}_DESC.txt".format(dst_dic=desc_directory, base_file_name=os.path.basename(file_path))
    np.savetxt(file_path, np_desc, delimiter=',')
    print("Desc Saved {file_path}".format(file_path=file_path))
    

if __name__ == '__main__':
    try:
        is_cuda_available = torch.cuda.is_available()
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        threads_no = 15
        print("Device: {val}".format(val=device))
        print("CPU threads numer: {threads_no}".format(threads_no=threads_no))
#%% Active region detection 
        imagePaths = ["{directory}/{x}".format(x=x, directory = main_image_directory)
                      for x in os.listdir(main_image_directory) if x.endswith(".jpg")]
        pool = Pool(threads_no)
        freeze_support()        
        start = time.time()
        active_region_detected_images = pool.map(detect_active_regions, imagePaths)
        # pool.map(detect_active_regions, imagePaths)
        pool.close()
        pool.join()
        #logSlack("Active regions detected")
#%% Generate descriptors
        descriptors = []
        for active_region_detected_image in active_region_detected_images:
            descriptor = calculate_descriptor(active_region_detected_image, device)
            max_descriptor_val = descriptor.max().item() if descriptor.max().item() > max_descriptor_val else max_descriptor_val 
            descriptors.append(descriptor)
        for i in range(0, len(descriptors)):
            descriptors[i] = torch.div(descriptors[i], max_descriptor_val)

# %% run autoencoder learning
        num_epochs = 10
        # batch_size = 30
        learning_rate = 1e-3
        torch.cuda.empty_cache()
        model = autoencoder().cuda()        
        summary(model, (1,4096))
        if os.path.exists("model.pdf"):
            os.remove("model.pdf")
        criterion = nn.MSELoss()
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=1e-5)
        for epoch in range(num_epochs):
            for data in descriptors:
                desc = torch.tensor(data, dtype=torch.float32, device=device).view(1, 1, len(data))
                # ===================forward=====================
                output = model(desc)
                if not os.path.exists("model.pdf"):
                    dot = make_dot(output.mean(), params=dict(model.named_parameters())) # shows graph
                    dot.render("model", format="pdf", cleanup=True)
                loss = criterion(output, desc)
                # ===================backward====================
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
            # ===================log========================
            print('epoch [{}/{}], loss:{:.4f}'.format(epoch+1, num_epochs, loss.data.item()))
        torch.save(model.state_dict(), './conv_autoencoder.pth')
#%% Get encoded hashes        
        encodedModel = model.encoded
        torch.cuda.empty_cache()
        for i, data in zip(range(0, len(imagePaths)), descriptors):
            desc = torch.tensor(data, dtype=torch.float32, device=device).view(1, 1, len(data))
            #print(img)
            output_hash = encodedModel(desc)
            #print(output)
            file_path = imagePaths[i]
            save_encoded_hash(output_hash.flatten(), file_path)
    except Exception as e:
        logSlack(str(e))
        raise
